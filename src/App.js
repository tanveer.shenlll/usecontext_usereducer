import "./App.css";
import Userlist from "./component/Userlist";
import { UserContext, UserContextProvider } from "./utils/UserContext";
import React, { useContext } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  const getUserData = useContext(UserContext);
  console.log("getUserData @ App", getUserData);
  return (
    <UserContextProvider>
      <Userlist />
    </UserContextProvider>
  );
}

export default App;
