import * as ACTION_TYPES from "../actions/action_types";
// import * as ACTION_TYPES from "../actions/action_types";


  

  export const userDetailsReducer = (state, action) => {
    switch (action.type) {
        case ACTION_TYPES.CALL_API: {
            return {
                ...state,
                loading: true,
            };
        }
        case ACTION_TYPES.SUCCESS: {
            return {
                ...state,
                loading: false,
                userDetails: action.data,
            };
        }
        case ACTION_TYPES.ERROR: {
            return {
                ...state,
                loading: false,
                error: action.error,
            };
        }
    }
};