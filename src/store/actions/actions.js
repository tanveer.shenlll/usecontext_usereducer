import * as ACTION_TYPES from './action_types';
export const list_all = (data) => {
    return {
      type: ACTION_TYPES.GET_ALL_LIST,
      list: data,
    };
  };
  
  export const logout = () => {
    return {
      type: ACTION_TYPES.LOGOUT,
    };
  };