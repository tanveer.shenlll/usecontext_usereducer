import React, {
  Children,
  useEffect,
  useReducer,
  useState,
  createContext,
} from "react";
import axios from "axios";
import { userDetailsReducer } from "../store/reducers/userDetailsReducer";
import * as ACTION_TYPES from "../store/actions/action_types";
// import Context from "./utils/context";
import App from "../App";
const initialState = {
  userDetails: "",
  loading: false,
  error: null,
};
const UserContext = createContext();
// {"id":1,"email":"george.bluth@reqres.in","first_name":"George","last_name":"Bluth","avatar":"https://reqres.in/img/faces/1-image.jpg"}
const UserContextProvider = ({ children }) => {
  const [userData, setUserData] = useState(
    [{
      name:'artz', 
    }]
  );
  const [state, dispatch] = useReducer(userDetailsReducer, initialState);
  const { userDetails, loading, error } = state;
  console.log("userDetails UserContext aditi shankar", userDetails);
  console.log("user Data aa ra", userData);

  const getUsers = async () => {
    let response = await axios.get("https://reqres.in/api/users?page=1");

    if (response.status == 200) {
      dispatch({ type: ACTION_TYPES.SUCCESS, data: response.data.data });
      return;
    }
    dispatch({ type: ACTION_TYPES.ERROR, error: response.error });
  };

  useEffect(() => {
    console.log("inside effecr xxxxxxxxx UserContext", userDetails);
    // dispatch({ type: ACTION_TYPES.CALL_API });

    if (!userDetails) {
      getUsers();
    }
  }, []);

  useEffect(() => {
    console.log("userDetails @useContext", userDetails);
    console.log("Array.isArray(userDetails)", Array.isArray(userDetails));
    if (Array.isArray(userDetails)) {
      console.log("userDetails @toye ", typeof userDetails);
      setUserData(userDetails);
    }
  }, [userDetails]);

  return (
    <UserContext.Provider
      value={{
        userDetails,
      }}
        // value={{
        //   userData
        // }}
    >
      {children}
    </UserContext.Provider>
  );
};

export { UserContextProvider, UserContext };
