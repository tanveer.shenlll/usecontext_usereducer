import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function CustomCard(props) {
  return (
    <Card style={{ width: '18rem', margin:'10px' ,paddingLeft:'20px'}}  class='col-md-6'>
      <Card.Img variant="top" src={props.img_src} />
      <Card.Body>
        <Card.Title>{props.user}</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default CustomCard;